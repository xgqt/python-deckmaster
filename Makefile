# This file is part of python-deckmaster.

# python-deckmaster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# python-deckmaster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with python-deckmaster.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


PYTHON          := python3
PIP             := pip

DOXYGEN         := doxygen
PYTEST          := pytest
TWINE           := twine

FIND            := find
MKDIR           := mkdir -p
RM              := rm -f
RMDIR           := $(RM) -r

PWD             ?= $(shell pwd)
DOCS            := $(PWD)/docs
EXTRAS          := $(PWD)/extras
SCRIPTS         := $(PWD)/scripts
SRC             := $(PWD)/src
TESTS           := $(PWD)/tests
XDG             := $(EXTRAS)/xdg

REQUIREMENTS    := $(PWD)/requirements.txt

APPLICATIONS    := ${XDG_DATA_HOME}/applications


build-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py build

.PHONY: build
build: build-deckmaster

.PHONY: compile
compile: build


bdist-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py bdist

sdist-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py sdist

wheel-%:
	cd $(SRC)/$(*) && $(PYTHON) $(SRC)/$(*)/setup.py bdist_wheel

dist-%:
	$(MAKE) sdist-$(*) wheel-$(*)

.PHONY: dist
dist: dist-deckmaster

.PHONY: pkg
pkg: clean build dist

upload-%:
	$(TWINE) upload $(SRC)/$(*)/dist/*

.PHONY: upload
upload: upload-deckmaster


.PHONY: pip-requirements
pip-requirements:
	$(PIP) install --user -r $(REQUIREMENTS)

pip-install-%:
	$(PIP) install --user -e $(SRC)/$(*)

$(APPLICATIONS):
	$(MKDIR) $(APPLICATIONS)

install-desktop-%: $(APPLICATIONS)
	cp $(XDG)/$(*).desktop $(APPLICATIONS)

.PHONY: install
install: pip-install-deckmaster install-desktop-deckmaster


.PHONY: run
run: compile
run:
	$(PYTHON) $(SCRIPTS)/run.py --debug --gui


pip-remove-%:
	$(PIP) uninstall --yes $(*)

remove: pip-remove-deckmaster


.PHONY: clean-docs
clean-docs:
	$(RMDIR) $(DOCS)/artifacts

docs/artifacts:
	$(MKDIR) $(DOCS)/artifacts
	$(DOXYGEN) $(DOCS)/doxygen.config

.PHONY: docs
docs: docs/artifacts


.PHONY: showcase
showcase:
	$(MAKE) -C $(EXTRAS)/showcase


.PHONY: test
test:
	cd $(TESTS) && $(PYTEST) --verbose


clean-%:
	$(FIND) $(SRC) -name $(*) -exec $(RMDIR) {} +

.PHONY: clean-cache
clean-cache:
	$(MAKE) clean-__pycache__
	$(MAKE) clean-build
	$(MAKE) clean-dist

clean: clean-docs clean-cache
