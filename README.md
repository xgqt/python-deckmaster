# Deckmaster


## About

TCG/CCG deck creation tool.


## Dependencies

- Run
  - PyQt5
  - requests
  - sqlalchemy

- Build
  - gmake
  - pip
  - Documentation
    - doxygen
  - Test
    - pytest


## Installation

### From pip

Deckmaster on PyPi: https://pypi.org/project/deckmaster/

``` shell
pip install --user --verbose deckmaster
```

### Manual

``` shell
make clean install
```


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License

SPDX-License-Identifier: GPL-3.0-only
