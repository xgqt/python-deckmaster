#+TITLE: Learning Resources

#+AUTHOR: Maciej Barć
#+LANGUAGE: en

#+STARTUP: showall inlineimages
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: black


* SQL

  - ORM Systems in Python (Polish version)
    [[https://python101.readthedocs.io/pl/latest/bazy/orm/]]

  - SQLAlchemy ORM
    [[https://docs.sqlalchemy.org/en/14/orm/]]


* Qt

  - Official "Qt for Python" documentation
    https://doc.qt.io/qtforpython-5/
