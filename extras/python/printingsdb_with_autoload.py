#!/usr/bin/env python3


"""
Interaction with database provided by MTGJSON.
""" """

This file is part of python-deckmaster.

python-deckmaster is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

python-deckmaster is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-deckmaster.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import os

# from sqlalchemy.sql import select

from sqlalchemy import (
    Column,
    # ForeignKey,
    Float,
    Integer,
    Date,
    Table,
    MetaData,
    String,
    create_engine
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from . import __cache_dir__


class Connection():
    """Connection to "AllPrintings.sqlite" database."""

    fallback_printingsdb_path = "sqlite:///.cache/AllPrintings.sqlite"

    def __init__(self, printingsdb_path=None, debug=False):

        if printingsdb_path:
            self.dir = printingsdb_path
        else:
            if os.path.exists(".cache/AllPrintings.sqlite"):
                self.dir = ".cache"
            else:
                self.dir = __cache_dir__

        self.connection_string = f"sqlite:///{self.dir}/AllPrintings.sqlite"

        if debug:
            print(f"connection_string = {self.connection_string}")

        self.engine = create_engine(self.connection_string, echo=debug)
        self.metadata = MetaData()
        self.connection = self.engine.connect()


class Session():
    """Session of a connection to "AllPrintings.sqlite" database."""

    def __init__(self, debug=False):

        self.db_session = sessionmaker(bind=Connection(debug=debug).engine)
        self.session = self.db_session()


BaseModel = declarative_base()


class Sets(BaseModel):
    """The "sets" table."""

    __table__ = Table(
        "sets",
        BaseModel.metadata,
        autoload=True,
        autoload_with=Connection().engine
    )


class Cards(BaseModel):
    """The "cards" table."""

    __table__ = Table(
        "cards",
        BaseModel.metadata,
        autoload=True,
        autoload_with=Connection().engine
    )


# session.query(Sets).all()[0].name
# session.query(Cards).all()[0].name
