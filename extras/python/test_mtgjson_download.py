#!/usr/bin/env python3


"""
Test out downloading from https://mtgjson.com/
Using compressed SQLite DB from
https://mtgjson.com/api/v5/AllPrintings.sqlite.zip
""" """

This file is part of python-deckmaster.

python-deckmaster is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

python-deckmaster is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-deckmaster.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


import requests
import zipfile

from os import path


def main():

    remote_url = "https://mtgjson.com/api/v5/AllPrintings.sqlite.zip"
    local_file = path.basename(remote_url)
    extracted_file = path.splitext(local_file)[0]

    try:
        with requests.get(remote_url, stream=True) as r:
            r.raise_for_status()
            with open(local_file, "wb") as f:
                print(f"Downloading {local_file} from {remote_url} ...")
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
    except Exception:
        raise Exception(f"Could not download {local_file} from {remote_url}")

    try:
        with zipfile.ZipFile(local_file, "r") as z:
            print(f"Extracting {local_file} to {extracted_file} ...")
            z.extractall(".")
    except Exception:
        raise Exception(f"Could not extract {local_file} to {extracted_file}")


if __name__ == "__main__":
    main()
