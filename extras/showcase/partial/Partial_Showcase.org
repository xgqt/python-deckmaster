#+TITLE: Dokumntacja Cząstkowa

#+AUTHOR: Maciej Barć
#+LANGUAGE: pl

#+STARTUP: showall inlineimages
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: black


* Status

  Na razie aplikacja nie ma za dużo z frontu, zająłem sie backendem.

  Odnośnie fontu: staram się przekształcić moją wizję używająć PyQt5
  (zamiast GiRepository).

  W tym momncie mam klasy do zapisaywanie talii w formacie JSON (=deck_json.py=)
  i przetwarzania bazy dostępnych kart (=printingsdb.py=).


* Pokaz DeckJSON

  Na zrzucie ekranu: walidacja czy podany jest odpowiedni board.

  [[./deck_json.jpg]]


* Upstream

  Repozytorium z kodem jest dostępne na GitLabie pod adresem
  [[https://gitlab.com/xgqt/python-deckmaster]]
