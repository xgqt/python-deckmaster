# -*- conf -*-

# Package
PyQt5
requests
sqlalchemy

# Packaging
twine
wheel

# Tests
coverage
pytest
tox
