#!/usr/bin/env python3


"""
Tests of the "deck_json" module.
""" """

This file is part of python-deckmaster.

python-deckmaster is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

python-deckmaster is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with python-deckmaster.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License
SPDX-License-Identifier: GPL-3.0-only
"""


from deckmaster.deck_json import (
    DeckJSON,
    default_contents
)


def test_default_contents():

    assert DeckJSON().contents == default_contents


def test_set_name():

    name = "asd"
    deck = DeckJSON()

    deck.set_name(name)
    assert deck.contents["name"] == name


def test_add_and_remove_card():

    deck = DeckJSON()

    deck.add_card("mainboard", "000")
    assert deck.contents["mainboard"] == [{"uuid": "000", "amount": 1}]

    deck.remove_card("mainboard", "000")
    assert deck.contents["mainboard"] == []
